package com.publicissapient.casestudy.ecomm.beans;

import java.sql.Date;
import java.sql.Timestamp;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Product {
    
    @JsonProperty("product_code")
    private Long prodCode;
    @JsonProperty("product_name")
    private String prodName;
    @JsonProperty("product_type")
    private String prodType;
    @JsonProperty("product_color")
    private String prodColor;
    @JsonProperty("product_brand")
    private String prodBrand;
    @JsonProperty("product_size")
    private Double prodSize;
    @JsonProperty("product_created_by_user")
    private String prodUserBy;
    @JsonProperty("product_active_flag")
    private String prodActiveFlag;
    @JsonProperty("subcategory_code")
    private Long subCateCode;
    private Timestamp prodCreatedDate = new Timestamp(new java.util.Date().getTime());
}
