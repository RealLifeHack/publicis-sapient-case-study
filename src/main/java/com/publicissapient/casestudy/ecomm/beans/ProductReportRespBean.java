package com.publicissapient.casestudy.ecomm.beans;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
public class ProductReportRespBean {

    private String prodName;
    private String prodBrand;
    private String prodCateName;
    private String prodSubCateName;
    private String prodType;
    private String prodColor;
    private Integer prodSize;
    private Double prodPrice;
    private String prodCreatedByUser;
    
    

    
}
