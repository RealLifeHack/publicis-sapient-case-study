package com.publicissapient.casestudy.ecomm.beans;

import java.sql.Date;
import java.sql.Timestamp;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class SubCategories {

    @JsonProperty("subcategory_code")
    private Integer subCateCode;
    @JsonProperty("subcategory_name")
    private String subCateName;
    @JsonProperty("subcategory_type")
    private String subCateType;
    @JsonProperty("subcategory_active_flag")
    private String subCateActive;
    @JsonProperty("subcategory_created_by")
    private String subCateUserBy;
    @JsonProperty("category_code")
    private Integer cateCode;

    private Timestamp subCateCreatedDate = new Timestamp(new java.util.Date().getTime());

}
