package com.publicissapient.casestudy.ecomm.beans;

import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;

import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Categories {

    @JsonProperty("category_code")
    @NotEmpty(message = "Please provide a category_code")
    private Integer cateCode;
    @JsonProperty("category_name")
    @NotEmpty(message = "Please provide a category_name")
    private String cateName;
    @JsonProperty("category_type")
    @NotEmpty(message = "Please provide a category_type")
    private String cateType;
    @JsonProperty("category_active_flag")
    @NotEmpty(message = "Please provide a category_active_flag")
    private String cateActiveFlag;
    @JsonProperty("category_created_by_user")
    private String cateUserBy;

    private Timestamp cateCreatedDate = new Timestamp(new java.util.Date().getTime());
}
