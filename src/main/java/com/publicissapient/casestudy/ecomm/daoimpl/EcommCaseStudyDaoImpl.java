package com.publicissapient.casestudy.ecomm.daoimpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import com.publicissapient.casestudy.ecomm.beans.AllProductDetails;
import com.publicissapient.casestudy.ecomm.beans.Categories;
import com.publicissapient.casestudy.ecomm.beans.Product;
import com.publicissapient.casestudy.ecomm.beans.ProductReportRespBean;
import com.publicissapient.casestudy.ecomm.beans.SubCategories;
import com.publicissapient.casestudy.ecomm.dao.IEcommCaseStudyDao;
import com.publicissapient.casestudy.ecomm.mapper.ProductReportRowMapper;

@Repository
public class EcommCaseStudyDaoImpl implements IEcommCaseStudyDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private DataSourceTransactionManager dataSourceTransactionManager;

    @Value("${categories.insert}")
    private String insertCateSql;

    @Value("${subcategories.insert}")
    private String insertSubCateSql;

    @Value("${product.insert}")
    private String insertProductSql;

    @Value("${reportby.color.query}")
    private String searchByColor;
    
    @Value("${reportby.brand.query}")
    private String searchByBrand;
    
    @Value("${reportby.size.query}")
    private String searchBySize;
    
    @Value("${reportby.price.query}")
    private String searchByPrice;
    
    @Value("${reportby.seller.query}")
    private String searchBySeller;
    
    @Value("${reportby.seller.count.query}")
    private String searchBySellerCount;
    
    

    @Override
    public String saveCategories(Categories categories) {
        int status = 0;
        String msg = "Record Not Inserted!!";
        TransactionDefinition txDef = new DefaultTransactionDefinition();
        TransactionStatus transactionStatus = dataSourceTransactionManager.getTransaction(txDef);
        try {
            status = jdbcTemplate.update(insertCateSql,
                    new Object[] { categories.getCateCode(), categories.getCateName(), categories.getCateType(),
                            categories.getCateActiveFlag(), categories.getCateUserBy(),
                            categories.getCateCreatedDate() });

            if (status == 1) {
                msg = "Record Inserted!!";
            }

            dataSourceTransactionManager.commit(transactionStatus);
        } catch (RuntimeException e) {
            dataSourceTransactionManager.rollback(transactionStatus);
        }

        return msg;
    }

    @Override
    public String saveSubCategories(SubCategories subCategories) {
        int status = 0;
        String msg = "Record Not Inserted!!";
        TransactionDefinition txDef = new DefaultTransactionDefinition();
        TransactionStatus transactionStatus = dataSourceTransactionManager.getTransaction(txDef);

        try {
            status = jdbcTemplate.update(insertSubCateSql,
                    new Object[] { subCategories.getSubCateCode(), subCategories.getSubCateName(),
                            subCategories.getSubCateType(), subCategories.getSubCateActive(),
                            subCategories.getSubCateUserBy(), subCategories.getSubCateCreatedDate(),
                            subCategories.getCateCode() });

            if (status == 1) {
                msg = "Record Inserted!!";
            }
            dataSourceTransactionManager.commit(transactionStatus);
        } catch (RuntimeException e) {
            dataSourceTransactionManager.rollback(transactionStatus);
        }

        return msg;

    }

    @Override
    public String saveProduct(Product product) {
        int status = 0;
        String msg = "Record Not Inserted!!";
        TransactionDefinition txDef = new DefaultTransactionDefinition();
        TransactionStatus transactionStatus = dataSourceTransactionManager.getTransaction(txDef);
        try {
            status = jdbcTemplate.update(insertProductSql,
                    new Object[] { product.getProdCode(), product.getProdName(), product.getProdType(),
                            product.getProdColor(), product.getProdBrand(), product.getProdSize(),
                            product.getProdCreatedDate(), product.getProdActiveFlag(), product.getSubCateCode() });

            if (status == 1) {
                msg = "Record Inserted!!";
            }
            dataSourceTransactionManager.commit(transactionStatus);
        } catch (RuntimeException e) {
            dataSourceTransactionManager.rollback(transactionStatus);
        }

        return msg;
    }

    @Override
    public List<AllProductDetails> getAllProduct() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<ProductReportRespBean> getProductByColor(String color) {

        List<ProductReportRespBean> list=null;
        try {
            list = jdbcTemplate.query(searchByColor, new Object[] { color },
                    new ProductReportRowMapper());
            
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
        
        return list;
    }

    @Override
    public List<ProductReportRespBean> getProductBySize(int size) {
        List<ProductReportRespBean> list=null;
        try {
            list = jdbcTemplate.query(searchBySize, new Object[] { size },
                    new ProductReportRowMapper());
            
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
        
        return list;
    }

    @Override
    public List<ProductReportRespBean> getProductByBrand(String brand) {
        List<ProductReportRespBean> list=null;
        try {
            list = jdbcTemplate.query(searchByBrand, new Object[] { brand },
                    new ProductReportRowMapper());
            
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
        
        return list;
    }

    @Override
    public List<ProductReportRespBean> getProductByPrice(double price) {
        List<ProductReportRespBean> list=null;
        try {
            list = jdbcTemplate.query(searchByPrice, new Object[] { price },
                    new ProductReportRowMapper());
            
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
        
        return list;
    }

    @Override
    public List<AllProductDetails> getProductBySku() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<ProductReportRespBean> getProductBySeller(String sellerName) {
        List<ProductReportRespBean> list=null;
        try {
            list = jdbcTemplate.query(searchBySeller, new Object[] { sellerName },
                    new ProductReportRowMapper());
            
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
        
        return list;
    }

    @Override
    public int getProductCountBySeller(String sellerName) {
        int count=0;
        try {
            count = jdbcTemplate.queryForObject(searchBySellerCount, new Object[] { sellerName },
                    Integer.class);
            
        } catch (EmptyResultDataAccessException e) {
            return 0;
        }
        
        return count;
    }

    @Override
    public String deactivateProduct(String deactivateFlag) {
        // TODO Auto-generated method stub
        return null;
    }

}
