package com.publicissapient.casestudy.ecomm.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;

import com.publicissapient.casestudy.ecomm.beans.ProductReportRespBean;

public class ProductReportRowMapper implements RowMapper<ProductReportRespBean> {
    
   // private List<ProductReportRespBean> list=new LinkedList<ProductReportRespBean>();

    @Override
    public ProductReportRespBean mapRow(ResultSet rs, int rowNum) throws SQLException {
        
        System.out.println("############");
        ProductReportRespBean productReportRespBean=new ProductReportRespBean();
        
        productReportRespBean.setProdName(rs.getString(1));
        productReportRespBean.setProdBrand(rs.getString(2));
        productReportRespBean.setProdCateName(rs.getString(3));
        productReportRespBean.setProdSubCateName(rs.getString(4));
        productReportRespBean.setProdType(rs.getString(5));
        productReportRespBean.setProdColor(rs.getString(6));
        productReportRespBean.setProdSize(rs.getInt(7));
        productReportRespBean.setProdPrice(rs.getDouble(8));
        productReportRespBean.setProdCreatedByUser(rs.getString(9));
        
        System.out.println("##rs.getString(1)::"+rs.getString(1));
        
        //list.add(productReportRespBean);
        
        return productReportRespBean;
        
    }
}
