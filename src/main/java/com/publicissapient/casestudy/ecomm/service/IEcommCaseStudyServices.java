package com.publicissapient.casestudy.ecomm.service;

import java.util.List;

import com.publicissapient.casestudy.ecomm.beans.AllProductDetails;
import com.publicissapient.casestudy.ecomm.beans.Categories;
import com.publicissapient.casestudy.ecomm.beans.Product;
import com.publicissapient.casestudy.ecomm.beans.ProductReportRespBean;
import com.publicissapient.casestudy.ecomm.beans.SubCategories;

public interface IEcommCaseStudyServices {
    
    public String saveCategories(Categories categories);
    public String saveSubCategories(SubCategories subCategories);
    public String saveProduct(Product product);
    public List<AllProductDetails> getAllProduct();
    
    public List<ProductReportRespBean> getProductByColor(String color);
    public List<ProductReportRespBean> getProductBySize(int size);
    public List<ProductReportRespBean> getProductByBrand(String brand);
    public List<ProductReportRespBean> getProductByPrice(double price);
    public List<AllProductDetails> getProductBySku();
    public List<ProductReportRespBean> getProductBySeller(String sellerName);
    public int getProductCountBySeller(String sellerName);
    
    
    public String deactivateProduct(String deactivateFlag);//For Audit
}
