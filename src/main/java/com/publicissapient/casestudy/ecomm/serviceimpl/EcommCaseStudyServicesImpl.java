package com.publicissapient.casestudy.ecomm.serviceimpl;

import java.util.List;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.publicissapient.casestudy.ecomm.beans.AllProductDetails;
import com.publicissapient.casestudy.ecomm.beans.Categories;
import com.publicissapient.casestudy.ecomm.beans.Product;
import com.publicissapient.casestudy.ecomm.beans.ProductReportRespBean;
import com.publicissapient.casestudy.ecomm.beans.SubCategories;
import com.publicissapient.casestudy.ecomm.dao.IEcommCaseStudyDao;
import com.publicissapient.casestudy.ecomm.repository.EcommDaoRepository;
import com.publicissapient.casestudy.ecomm.service.IEcommCaseStudyServices;

@Service

public class EcommCaseStudyServicesImpl implements IEcommCaseStudyServices{
    
    @Autowired
    private IEcommCaseStudyDao iEcommCaseStudyDao;

    @Override
    @Transactional
    public String saveCategories(Categories categories) {
        // TODO Auto-generated method stub
        return iEcommCaseStudyDao.saveCategories(categories);
    }

    @Override
    @Transactional
    public String saveSubCategories(SubCategories subCategories) {
        // TODO Auto-generated method stub
        return iEcommCaseStudyDao.saveSubCategories(subCategories);
    }

    @Override
    @Transactional
    public String saveProduct(Product product) {
        // TODO Auto-generated method stub
        return iEcommCaseStudyDao.saveProduct(product);
    }

    @Override
    public List<AllProductDetails> getAllProduct() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    @Transactional(readOnly=true)
    @Cacheable(value="productsCache", key="#p0")
    public List<ProductReportRespBean> getProductByColor(String color) {
        
        return iEcommCaseStudyDao.getProductByColor(color);
    }

    @Override
    @Transactional(readOnly=true)
    public List<ProductReportRespBean> getProductBySize(int size) {
        
        return iEcommCaseStudyDao.getProductBySize(size);
    }

    @Override
    @Transactional(readOnly=true)
    public List<ProductReportRespBean> getProductByBrand(String brand) {
        
        return iEcommCaseStudyDao.getProductByBrand(brand);
    }

    @Override
    @Transactional(readOnly=true)
    public List<ProductReportRespBean> getProductByPrice(double price) {
        
        return iEcommCaseStudyDao.getProductByPrice(price);
    }

    @Override
    public List<AllProductDetails> getProductBySku() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    @Transactional(readOnly=true)
    public List<ProductReportRespBean> getProductBySeller(String sellerName) {
        // TODO Auto-generated method stub
        return iEcommCaseStudyDao.getProductBySeller(sellerName);
    }
    
    @Override
    @Transactional(readOnly=true)
    public int getProductCountBySeller(String sellerName) {
        // TODO Auto-generated method stub
        return iEcommCaseStudyDao.getProductCountBySeller(sellerName);
    }

    @Override
    public String deactivateProduct(String deactivateFlag) {
        // TODO Auto-generated method stub
        return null;
    }

}
