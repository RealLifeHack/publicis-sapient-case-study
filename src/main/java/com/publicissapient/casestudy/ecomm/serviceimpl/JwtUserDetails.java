package com.publicissapient.casestudy.ecomm.serviceimpl;

import java.util.ArrayList;

import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class JwtUserDetails implements UserDetailsService{

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        if ("dhirajchavan2912@gmail.com".equals(username)) {
            return new User("dhirajchavan2912@gmail.com", "$2a$10$Gcb/4QXU13DCbnnCf.BsR.ezrfT07jLHt9YSwEg0BuPfcqErb.CfW",
                    new ArrayList<>());
        } else {
            throw new UsernameNotFoundException("User not found with username: " + username);
        }
    }
}
