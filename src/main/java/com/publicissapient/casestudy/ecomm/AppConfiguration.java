package com.publicissapient.casestudy.ecomm;

import javax.annotation.Resource;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.catalina.servlets.WebdavServlet;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.init.DatabasePopulator;
import org.springframework.jdbc.datasource.init.DatabasePopulatorUtils;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.jndi.JndiTemplate;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.bind.annotation.RequestMapping;

import io.swagger.models.Contact;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@ComponentScan(basePackages="com.publicissapient.casestudy.ecomm.*")
@PropertySource(value = { "classpath:application.properties"})
@EnableSwagger2
@EnableCaching
@RequestMapping("/api/v1")
@EnableTransactionManagement
public class AppConfiguration {
    
    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2).select()
            .apis(RequestHandlerSelectors
                .basePackage("com.publicissapient.casestudy.ecomm.controller.*"))
            .paths(PathSelectors.regex("/.*"))
            .build().apiInfo(apiEndPointsInfo());
    }
    private ApiInfo apiEndPointsInfo() {
        return new ApiInfoBuilder().title("Saphient Ecomm Case Study")
            .description("Employee Management REST API")
            .contact(new springfox.documentation.service.Contact("Dhiraj Chavan", "www", "dhiraj.rchavan@gmail.com"))
            .license("Apache 2.0")
            .licenseUrl("http://www.apache.org/licenses/LICENSE-2.0.html")
            .version("1.0.0")
            .build();
    }
    
    @Bean(name = "dataSource")
    public DriverManagerDataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        //dataSource.setDriverClassName("org.h2.Driver");
//        dataSource.setDriverClassName("com.mysql.jdbc.Driver");
        dataSource.setUrl("jdbc:mysql://localhost:3306/ecomm");
        dataSource.setUsername("root");
        dataSource.setPassword("");

        // schema init
       // Resource initSchema = (Resource) new ClassPathResource("/schema-h2.sql");
        
        DatabasePopulator databasePopulator = new ResourceDatabasePopulator(new ClassPathResource("schema-h2.sql"));
        DatabasePopulatorUtils.execute(databasePopulator, dataSource);

        return dataSource;
    }
    
    //### Used for H2
    
//    @Bean
//    public DataSource dataSource() {
//        return new EmbeddedDatabaseBuilder()
//          .setType(EmbeddedDatabaseType.H2)
//          .addScript("classpath:jdbc/schema.sql")
//          .addScript("classpath:jdbc/test-data.sql").build();
//    }
    
    
    @Bean
    public DataSourceTransactionManager transactionManager() {
        final DataSourceTransactionManager transactionManager = new DataSourceTransactionManager(dataSource());
        return transactionManager;
    }
    
    @Bean()
    public JdbcTemplate ecommJdbcTemplate() throws NamingException {
        JdbcTemplate jdbcTemplate = new JdbcTemplate();
        jdbcTemplate.setDataSource(dataSource());
        return jdbcTemplate;
    }
    
//    @Bean
//    ServletRegistrationBean servletRegistrationBean() {
//        ServletRegistrationBean bean=new ServletRegistrationBean<>(new WebdavServlet());
//        bean.addUrlMappings("/h2-console/");
//        return bean;
//        
//    }
//    @Bean
//    public javax.sql.DataSource getEcommDatasource() throws NamingException {
//
//        JndiTemplate jndiTemplate = new JndiTemplate();
//        DataSource dataSource = (DataSource) jndiTemplate.lookup("");
//        return dataSource;
//    }
//    


}
