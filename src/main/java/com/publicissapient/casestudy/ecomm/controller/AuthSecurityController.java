package com.publicissapient.casestudy.ecomm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.publicissapient.casestudy.ecomm.beans.JwtRequestBean;
import com.publicissapient.casestudy.ecomm.beans.JwtResponseBean;
import com.publicissapient.casestudy.ecomm.service.IEcommCaseStudyServices;
import com.publicissapient.casestudy.ecomm.serviceimpl.JwtUserDetails;
import com.publicissapient.casestudy.ecomm.utility.JwtTockenUtil;

@RestController
@RequestMapping("/auth")
public class AuthSecurityController {
    

    @Autowired
    private AuthenticationManager authenticationManager;
    
   
    @Autowired
    private JwtTockenUtil jwtTokenUtil;

    @Autowired
    private JwtUserDetails userDetailsService;
    
    @RequestMapping({ "/test" })
    public String firstPage() {
        return "Good to start!!";
    }
    
    
    @RequestMapping(value = "/authenticate", method = RequestMethod.POST)
    public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtRequestBean authenticationRequest) throws Exception {

        System.out.println(authenticationRequest.getUsername()+""+authenticationRequest.getPassword());
        authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());

        final UserDetails userDetails = userDetailsService
                .loadUserByUsername(authenticationRequest.getUsername());

        final String token = jwtTokenUtil.generateToken(userDetails);

        return ResponseEntity.ok(new JwtResponseBean(token));
    }

    private void authenticate(String username, String password) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }

}
