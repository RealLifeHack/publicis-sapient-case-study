package com.publicissapient.casestudy.ecomm.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.publicissapient.casestudy.ecomm.beans.Categories;
import com.publicissapient.casestudy.ecomm.beans.JwtRequestBean;
import com.publicissapient.casestudy.ecomm.beans.JwtResponseBean;
import com.publicissapient.casestudy.ecomm.beans.Product;
import com.publicissapient.casestudy.ecomm.beans.ProductReportRespBean;
import com.publicissapient.casestudy.ecomm.beans.SubCategories;
import com.publicissapient.casestudy.ecomm.service.IEcommCaseStudyServices;
import com.publicissapient.casestudy.ecomm.serviceimpl.JwtUserDetails;
import com.publicissapient.casestudy.ecomm.utility.JwtTockenUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

//import com.publicissapient.casestudy.ecomm.dao.ICategorieRepoDao;
//import com.publicissapient.casestudy.ecomm.dao.ISubCatItemsRepo;
//import com.publicissapient.casestudy.ecomm.dao.ISubCategoriesRepo;
//import com.publicissapient.casestudy.ecomm.dto.ProductReqDto;


@RestController
@CrossOrigin
@RequestMapping("/ecomm")
public class EcommCaseStudyController {
    
    @Autowired
    private AuthenticationManager authenticationManager;
    
    @Autowired
    private IEcommCaseStudyServices iEcommCaseStudyServices;

    @Autowired
    private JwtTockenUtil jwtTokenUtil;

    @Autowired
    private JwtUserDetails userDetailsService;
    
    @RequestMapping({ "/test" })
    public String firstPage() {
        return "Good to start!!";
    }
    
    
    @RequestMapping(value = "/categories", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation("Add Categories")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "OK", response = String.class) })
    public ResponseEntity<String> addCategories(@Valid @RequestBody Categories categories,Errors errors) {
        
        return new ResponseEntity<>(iEcommCaseStudyServices.saveCategories(categories), HttpStatus.OK);

    }
    
    @RequestMapping(value = "/subcategories", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation("Add SubCategories")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "OK", response = String.class) })
    public ResponseEntity<String> addSubCategories(@RequestBody SubCategories subCategories) {

        return new ResponseEntity<>(iEcommCaseStudyServices.saveSubCategories(subCategories), HttpStatus.OK);

    }
    
    @RequestMapping(value = "/product", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation("Add Product")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "OK", response = String.class) })
    public ResponseEntity<String> addProduct(@RequestBody Product product) {

        return new ResponseEntity<>(iEcommCaseStudyServices.saveProduct(product), HttpStatus.OK);

    }
    
    
    @GetMapping(value = "/getreportbycolor", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation("Get product report by color")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "OK", response = List.class) })
    public ResponseEntity<List<ProductReportRespBean>> addProductByColor(@RequestParam("color") String color) {

        return new ResponseEntity<>(iEcommCaseStudyServices.getProductByColor(color), HttpStatus.OK);

    }
    
    @GetMapping(value = "/getreportbybrand", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation("Get product report by color")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "OK", response = List.class) })
    public ResponseEntity<List<ProductReportRespBean>> addProductByBrand(@RequestParam("brand") String brand) {

        return new ResponseEntity<>(iEcommCaseStudyServices.getProductByBrand(brand), HttpStatus.OK);

    }
    
    @GetMapping(value = "/getreportbysize", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation("Get product report by color")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "OK", response = List.class) })
    public ResponseEntity<List<ProductReportRespBean>> addProductBySize(@RequestParam("size") int size) {

        return new ResponseEntity<>(iEcommCaseStudyServices.getProductBySize(size), HttpStatus.OK);

    }
    
    @GetMapping(value = "/getreportbyprice", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation("Get product report by color")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "OK", response = List.class) })
    public ResponseEntity<List<ProductReportRespBean>> addProductBySize(@RequestParam("price") double price) {

        return new ResponseEntity<>(iEcommCaseStudyServices.getProductByPrice(price), HttpStatus.OK);

    }
    
    @GetMapping(value = "/getreportbyseller", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation("Get product report by color")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "OK", response = List.class) })
    public ResponseEntity<List<ProductReportRespBean>> addProductBy(@RequestParam("sellername") String sellerName) {

        return new ResponseEntity<>(iEcommCaseStudyServices.getProductBySeller(sellerName), HttpStatus.OK);

    }
    
    @GetMapping(value = "/getproductcountbyseller", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation("Get product report by color")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "OK", response = Integer.class) })
    public ResponseEntity<?> addProductCountBySeller(@RequestParam("sellername") String sellerName) {

        return new ResponseEntity<>(iEcommCaseStudyServices.getProductCountBySeller(sellerName), HttpStatus.OK);

    }
    
    

//    @RequestMapping(value = "/authenticate", method = RequestMethod.POST)
//    public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtRequestBean authenticationRequest) throws Exception {
//
//        System.out.println(authenticationRequest.getUsername()+""+authenticationRequest.getPassword());
//        authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());
//
//        final UserDetails userDetails = userDetailsService
//                .loadUserByUsername(authenticationRequest.getUsername());
//
//        final String token = jwtTokenUtil.generateToken(userDetails);
//
//        return ResponseEntity.ok(new JwtResponseBean(token));
//    }
//
//    private void authenticate(String username, String password) throws Exception {
//        try {
//            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
//        } catch (DisabledException e) {
//            throw new Exception("USER_DISABLED", e);
//        } catch (BadCredentialsException e) {
//            throw new Exception("INVALID_CREDENTIALS", e);
//        }
//    }

}
