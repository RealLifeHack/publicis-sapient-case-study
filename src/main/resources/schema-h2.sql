
DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories` (`cate_id` int NOT NULL,`cate_name` varchar(50),`cate_type` varchar(50),`cate_active` char,`cate_created_by` varchar(50),`cate_updated_by` datetime, PRIMARY KEY (cate_id));
DROP TABLE IF EXISTS `subcategories`;
create table subcategories
(
  subcate_id           int         not null,
  subcate_name         varchar(50) null,
  subcate_type         varchar(50) null,
  subcate_active       char        null,
  subcate_created_by   varchar(50) null,
  subcate_created_date datetime    null,
  cate_id              int         null,
  constraint subcategories_subcate_id_uindex
    unique (subcate_id)
);

alter table subcategories
  add primary key (subcate_id);

  
  DROP TABLE IF EXISTS `product`;
  create table product
(
	prod_id int not null,
	prod_name varchar(50) null,
	prod_type varchar(50) null,
	prod_color varchar(20) null,
	prod_brand varchar(50) null,
	prod_size int null,
	prod_price double null,
	prod_created_date datetime null,
	prod_active char null
);

create unique index product_prod_id_uindex
	on product (prod_id);

alter table product
	add constraint product_pk
		primary key (prod_id);

alter table product
	add subcate_id int null;
	
	
	DROP TABLE IF EXISTS `supplier`;
	create table supplier
(
	supp_id int not null,
	supp_name varchar(50) null,
	supp_city varchar(30) null,
	supp_created_by varchar(50) null
);

create unique index supplier_supp_id_uindex
	on supplier (supp_id);

alter table supplier
	add constraint supplier_pk
		primary key (supp_id);

		
		
		INSERT INTO ecomm.product (prod_id, prod_name, prod_type, prod_color, prod_brand, prod_size,prod_price, prod_created_date, prod_active, subcate_id) VALUES (1, 'SHIRT-1', 'FORMALS', 'WHITE', 'PARK', 42,999, '2020-04-14 01:16:23', 'Y', 1);
INSERT INTO ecomm.product (prod_id, prod_name, prod_type, prod_color, prod_brand, prod_size,prod_price, prod_created_date, prod_active, subcate_id) VALUES (2, 'SHIRT-2', 'CASUALS', 'WHITE', 'PARK', 42,1099, '2020-04-14 01:16:38', 'Y', 2);
INSERT INTO ecomm.product (prod_id, prod_name, prod_type, prod_color, prod_brand, prod_size,prod_price, prod_created_date, prod_active, subcate_id) VALUES (3, 'SHIRT-3', 'CASUALS', 'WHITE', 'PARK', 42,295, '2020-04-14 01:16:49', 'Y', 3);
INSERT INTO ecomm.product (prod_id, prod_name, prod_type, prod_color, prod_brand, prod_size,prod_price, prod_created_date, prod_active, subcate_id) VALUES (4, 'SHIRT-4', 'FORMALS', 'BLACK', 'INTEGITY', 42,599, '2020-04-14 01:17:39', 'Y', 1);
INSERT INTO ecomm.product (prod_id, prod_name, prod_type, prod_color, prod_brand, prod_size,prod_price, prod_created_date, prod_active, subcate_id) VALUES (5, 'SHIRT-5', 'CASUALS', 'BLACK', 'INTEGITY', 42,500, '2020-04-14 01:18:03', 'Y', 2);
INSERT INTO ecomm.product (prod_id, prod_name, prod_type, prod_color, prod_brand, prod_size,prod_price, prod_created_date, prod_active, subcate_id) VALUES (6, 'SHIRT-6', 'HALF-SLEEVES', 'BLACK', 'INTEGITY', 42,911, '2020-04-14 01:18:20', 'Y', 3);
INSERT INTO ecomm.product (prod_id, prod_name, prod_type, prod_color, prod_brand, prod_size,prod_price, prod_created_date, prod_active, subcate_id) VALUES (7, 'SHIRT-7', 'FORMALS', 'SKY BLUE', 'PETER ENGLAND', 42,950, '2020-04-14 01:18:53', 'Y', 1);
INSERT INTO ecomm.product (prod_id, prod_name, prod_type, prod_color, prod_brand, prod_size,prod_price, prod_created_date, prod_active, subcate_id) VALUES (8, 'SHIRT-8', 'CASUALS', 'BLUE', 'PARK', 42,1090, '2020-04-14 01:14:33', 'Y', 2);
INSERT INTO ecomm.product (prod_id, prod_name, prod_type, prod_color, prod_brand, prod_size,prod_price, prod_created_date, prod_active, subcate_id) VALUES (9, 'SHIRT-9', 'HALF-SLEEVES', 'SKY BLUE', 'PETER ENGLAND', 42,1299, '2020-04-14 01:19:29', 'Y', 3);




INSERT INTO ecomm.subcategories (subcate_id, subcate_name, subcate_type, subcate_active, subcate_created_by, subcate_created_date, cate_id) VALUES (1, 'FORMALS', 'MEN', 'Y', 'DHIRAJ', '2020-04-14 01:11:11', 1);
INSERT INTO ecomm.subcategories (subcate_id, subcate_name, subcate_type, subcate_active, subcate_created_by, subcate_created_date, cate_id) VALUES (2, 'CASUALS', 'MEN', 'Y', 'DHIRAJ', '2020-04-14 01:11:45', 1);
INSERT INTO ecomm.subcategories (subcate_id, subcate_name, subcate_type, subcate_active, subcate_created_by, subcate_created_date, cate_id) VALUES (3, 'HALF-SLEEVES', 'MEN', 'Y', 'DHIRAJ', '2020-04-14 01:11:58', 1);
INSERT INTO ecomm.subcategories (subcate_id, subcate_name, subcate_type, subcate_active, subcate_created_by, subcate_created_date, cate_id) VALUES (4, 'FORMALS', 'WOMEN', 'Y', 'DHIRAJ', '2020-04-14 01:12:45', 2);
INSERT INTO ecomm.subcategories (subcate_id, subcate_name, subcate_type, subcate_active, subcate_created_by, subcate_created_date, cate_id) VALUES (5, 'CASUALS', 'WOMEN', 'Y', 'DHIRAJ', '2020-04-14 01:13:11', 2);
INSERT INTO ecomm.subcategories (subcate_id, subcate_name, subcate_type, subcate_active, subcate_created_by, subcate_created_date, cate_id) VALUES (6, 'HALF-SLEEVES', 'WOMEN', 'Y', 'DHIRAJ', '2020-04-14 01:13:26', 2);



INSERT INTO ecomm.categories (cate_id, cate_name, cate_type, cate_active, cate_created_by, cate_updated_by) VALUES (1, 'MEN', 'CLOTH', 'Y', 'DHIRAJ', '2020-04-14 01:09:39');
INSERT INTO ecomm.categories (cate_id, cate_name, cate_type, cate_active, cate_created_by, cate_updated_by) VALUES (2, 'WOMEN', 'CLOTH', 'Y', 'DHIRAJ', '2020-04-14 01:09:58');

